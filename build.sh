go get -t github.com/godbus/dbus
go get -t github.com/jteeuwen/go-pkg-rss
go get -t github.com/microcosm-cc/bluemonday
go get -t gopkg.in/alecthomas/kingpin.v1
go get -t gopkg.in/yaml.v2

go build bitbucket.com/jatone/bulletin/...
go build -o bin/bulletin bitbucket.com/jatone/bulletin


# debugging/profiling
# install graphviz
# go tool pprof --web bin/bulletin http://localhost:6060/debug/pprof/heap
# go tool pprof --web --seconds=5 bin/bulletin http://localhost:6060/debug/pprof/profile
