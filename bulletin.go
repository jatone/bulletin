package main

import "time"
import "fmt"
import "os"
import "os/user"
import "io/ioutil"
import "path/filepath"

import "runtime"
import _ "net/http/pprof"
import "net/http"
import "log"

import "github.com/godbus/dbus"
import rss "github.com/jteeuwen/go-pkg-rss"
import "github.com/microcosm-cc/bluemonday"
import "gopkg.in/yaml.v2"
import "gopkg.in/alecthomas/kingpin.v1"

import "bitbucket.com/jatone/bulletin/bulletin"
import "bitbucket.com/jatone/bulletin/notifications"

func main() {
  var urls []string
  var err error

  runtime.GOMAXPROCS(runtime.NumCPU())
  current, err := user.Current()

  if err != nil {
    fmt.Println(err)
    return
  }

  app := kingpin.New("bulletin", "Gnome RSS Feed Reader")
  app.Version("0.0.1")

  _name := `profile`
  _desc := `profile the application (defaults to port 6060)`

  profile := app.Flag(_name, _desc).Bool()

  _name = `profile-port`
  _desc = `port for profiling`

  profile_port := app.Flag(_name, _desc).Short('p').Default("6060").Int()

  _name = `feeds`
  _desc = `file containing the feeds to monitor`
  feeds := app.Arg(_name, _desc).Default(filepath.Join(current.HomeDir, ".local", "share", "bulletin", "feeds.yml")).ExistingFile()

  kingpin.MustParse(app.Parse(os.Args[1:]))

  if *profile {
    go func() {
      log.Println(http.ListenAndServe(fmt.Sprintf("localhost:%d", *profile_port), nil))
    }()
  }

  if urls, err = load(*feeds); err != nil {
    fmt.Println(err)
    return
  }

  begin(urls...)
}

func load(path string) ([]string, error) {
  var binary []byte
  var err error
  urls := []string{}

  fmt.Println("file", path)
  if binary, err = ioutil.ReadFile(path); err != nil {
    return urls, err
  }

  err = yaml.Unmarshal(binary, &urls)
  return urls, err
}

func begin(urls ...string) {
  conn, err := dbus.SessionBus()
  if err != nil {
    panic(err)
  }

  _, err = conn.RequestName("org.jambli.bulletin", dbus.NameFlagDoNotQueue)
  if err != nil {
    panic(err)
  }

  n := notifications.New(conn)

  fmt.Println(n.Capabilities())
  fmt.Println(n.ServerInformation())

  sink := bulletin.Feed {
    NewChannelSink: make(chan bulletin.NewChannelEvent, 10),
    NewItemSink: make(chan bulletin.NewItemEvent, 100),
  }

  db := rss.NewDatabase()
  ch := rss.NewDatabaseChannelHandler(db, sink)
  ih := rss.NewDatabaseItemHandler(db, sink)

  for _, url := range urls {
    feed := rss.NewWithHandlers(5, true, ch, ih)
    go bulletin.Poll(feed, url)
  }

  // Sanitize the RSS Feed Items
  p := bluemonday.NewPolicy()
  p.AllowStandardURLs()
  p.AllowElements("b", "i", "u")

  factory := notifications.RSSNotificationFactory {
    Application: "rss",
    ReplacesId: 0,
    Icon: "/usr/share/icons/gnome/scalable/mimetypes/application-rss+xml-symbolic.svg",
    TTL: int32(time.Second / time.Millisecond),
    Hints: map[string]dbus.Variant {
      "category": dbus.MakeVariant("im.received"),
    },
    Summarize: notifications.BasicSummary,
    Body: notifications.SanitizingTransformer(notifications.BasicBody, func(untrusted string) string {
      return p.Sanitize(untrusted)
    }),
  }

  for {
    select {
    case event := <- sink.NewItemSink:
      if _, err := n.Notify(factory.New(event.Feed, event.Channel, event.Item)); err != nil {
        fmt.Println(err)
      }
    case <- sink.NewChannelSink:
      // do nothing.
    }
  }
}
