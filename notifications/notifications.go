package notifications

import "github.com/godbus/dbus"

func New(conn * dbus.Conn) Notifications {
  return Notifications{obj: conn.Object("org.freedesktop.Notifications", "/org/freedesktop/Notifications"), conn: conn}
}

type ClosedEvent struct {
  Id uint32
  Reason uint32
}

type Notification struct {
  Application string
  ReplacesId uint32
  Icon string
  Summary string
  Body string
  Actions []string
  Hints map[string]dbus.Variant
  TTL int32
}

type serverInformation struct {
  Name string
  Vendor string
  Version string
  SpecVersion string
}

type Notifications struct {
  obj * dbus.Object
  conn * dbus.Conn
}

func (t Notifications) Capabilities() ([]string, error) {
  result := []string{}
  err := t.obj.Call("org.freedesktop.Notifications.GetCapabilities", 0).Store(&result)

  return result, err
}

func (t Notifications) ServerInformation() (serverInformation, error) {
  result := serverInformation{}
  err := t.obj.Call("org.freedesktop.Notifications.GetServerInformation", 0).Store(&result.Name, &result.Vendor, &result.Version, &result.SpecVersion)
  return result, err
}

func (t Notifications) Notify(n Notification) (uint32, error) {
  var id uint32

  err := t.obj.Call("org.freedesktop.Notifications.Notify", 0,
    n.Application,
    n.ReplacesId,
    n.Icon,
    n.Summary,
    n.Body,
    n.Actions,
    n.Hints,
    n.TTL).Store(&id)

  return id, err
}

func (t Notifications) CloseNotification(id uint32) error {
  call := t.obj.Call("org.freedesktop.Notifications.CloseNotification", 0, id)
  return call.Err
}

func (t * Notifications) NotificationClosed(c chan ClosedEvent) (error) {
  call := t.conn.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, "type='signal',path='/org/freedesktop/Notifications',interface='org.freedesktop.Notifications',sender='org.freedesktop.Notifications'")
  if call.Err != nil {
    return call.Err
  }

  signals := make(chan *dbus.Signal)
  t.conn.Signal(signals)

  go func() {
    for signal := range signals {
      if signal.Name != "org.freedesktop.Notifications.NotificationClosed" {
        continue
      }

      c <- ClosedEvent{Id: signal.Body[0].(uint32), Reason: signal.Body[1].(uint32)}
    }
  }()
  return nil
}
