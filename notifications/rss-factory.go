package notifications

import "fmt"

import "github.com/godbus/dbus"
import rss "github.com/jteeuwen/go-pkg-rss"

type Sanitizer func(string) string
type Transformer func(f * rss.Feed, c * rss.Channel, i * rss.Item) string

type RSSNotificationFactory struct {
  Application string
  ReplacesId uint32
  Icon string
  TTL int32
  Hints map[string]dbus.Variant
  Summarize Transformer
  Body Transformer
}

func (t RSSNotificationFactory) New(f * rss.Feed, c * rss.Channel, i * rss.Item) Notification {
  return Notification {
    Application: t.Application,
    ReplacesId: 0,
    Icon: t.Icon,
    Summary: t.Summarize(f, c, i),
    Body: t.Body(f, c, i),
    TTL: t.TTL,
    Hints: t.Hints,
  }
}

func BasicSummary(f * rss.Feed, c * rss.Channel, i * rss.Item) string {
 return fmt.Sprintf("New Item : %s %s", c.Title, i.Author.Name)
}

func BasicBody(f * rss.Feed, c * rss.Channel, i * rss.Item) string {
  return fmt.Sprintf("%s\n%s\n%s\n%s", i.Title, i.PubDate, i.Links[0].Href, i.Description)
}

func SanitizingTransformer(t Transformer, s Sanitizer) func(f * rss.Feed, c * rss.Channel, i * rss.Item) string {
  return func(f * rss.Feed, c * rss.Channel, i * rss.Item) string {
    return s(t(f, c, i))
  }
}
