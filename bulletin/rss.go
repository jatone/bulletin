package bulletin

import "fmt"
import "time"

import rss "github.com/jteeuwen/go-pkg-rss"

type NewChannelEvent struct {
  Feed *rss.Feed
  Channel *rss.Channel
}

type NewItemEvent struct {
  Feed *rss.Feed
  Channel *rss.Channel
  Item *rss.Item
}

type Feed struct {
  NewChannelSink chan NewChannelEvent
  NewItemSink chan NewItemEvent
}

func (t Feed) ProcessChannels(f *rss.Feed, channels []*rss.Channel) {
  for _, channel := range channels {
    t.NewChannelSink <- NewChannelEvent{Feed: f, Channel: channel}
  }
}

func (t Feed) ProcessItems(f *rss.Feed, channel *rss.Channel, items []*rss.Item) {
  for _, item := range items {
    t.NewItemSink <- NewItemEvent{Feed: f, Channel: channel, Item: item}
  }
}

func Poll(feed * rss.Feed, uri string) {
  for {
    if err := feed.Fetch(uri, nil); err != nil {
      fmt.Println("error retrieving", uri)
    }

    <-time.After(time.Duration(feed.SecondsTillUpdate()) * time.Millisecond)
  }
}
